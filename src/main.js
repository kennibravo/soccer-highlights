import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import * as firebase from 'firebase';

Vue.config.productionTip = false


const configOptions = {
  apiKey: "AIzaSyC4B3yE49M0-bzLqO0_f9_GbvUIEEPong0",
  authDomain: "soccer-highlights-99782.firebaseapp.com",
  databaseURL: "https://soccer-highlights-99782.firebaseio.com",
  projectId: "soccer-highlights-99782",
  storageBucket: "soccer-highlights-99782.appspot.com",
  messagingSenderId: "884966357003",
  appId: "1:884966357003:web:8c3d9ad9691cfc693eaf0e",
  measurementId: "G-Y6X9ZHKCR6"
};

firebase.initializeApp(configOptions);
firebase.auth().onAuthStateChanged(user => {
  store.dispatch('fetchUser', user);
});


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
