import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    videos: [""],
    formattedVideos: [],
    user: {
      loggedIn: false,
      data: null
    }
  },

  getters: {
    videos: state => {
      return state.videos;
    },
    formattedVideos: state => {
      return state.formattedVideos;
    },

    user: state => {
      return state.user
    }
  },
  mutations: {
    updateVideos: (state, payload) => {
      state.videos = payload;
    },
    updatedFormattedVideos: (state, payload) => {
      state.formattedVideos.push(payload);
    },
    setLoggedIn: (state, payload) => {
      state.user.loggedIn = payload;
    },

    setUser: (state, data) => {
      state.user.data = data;
    }
  },
  actions: {
    updateVideos: ({ commit }, payload) => {
      commit('updateVideos', payload);
    },
    updatedFormattedVideos: ({ commit }, payload) => {
      commit('updatedFormattedVideos', payload);
    },

    fetchUser: ({ commit }, user) => {
      commit("setLoggedIn", user !== null);

      if (user) {
        commit('setUser', {
          displayName: user.displayName,
          email: user.email,
        })
      }else{
        commit('setUser', null);
      }
    }
  },

});
